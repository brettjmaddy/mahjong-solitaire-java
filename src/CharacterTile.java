
import java.awt.*;
import java.util.HashMap;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

public class CharacterTile extends Tile
{
    protected char symbol;
    private static final HashMap<Character, String> symbolMap;
    private static Font font;
    
    
    
    
    public CharacterTile(char symbol)
    {  
        this.symbol = symbol;
        setToolTipText(toString());
    }
    
    static
    {
        symbolMap = new HashMap<>();
        symbolMap.put('1', Character.toString('\u4E00'));
        symbolMap.put('2', Character.toString('\u4E8C'));
        symbolMap.put('3', Character.toString('\u4E09'));
        symbolMap.put('4', Character.toString('\u56DB'));
        symbolMap.put('5', Character.toString('\u4E94'));
        symbolMap.put('6', Character.toString('\u516D'));
        symbolMap.put('7', Character.toString('\u4E03'));
        symbolMap.put('8', Character.toString('\u516B'));
        symbolMap.put('9', Character.toString('\u4E5D'));
        symbolMap.put('N', Character.toString('\u5317'));
        symbolMap.put('E', Character.toString('\u6771'));
        symbolMap.put('W', Character.toString('\u897F'));
        symbolMap.put('S', Character.toString('\u5357'));
        symbolMap.put('C', Character.toString('\u4E2D'));
        symbolMap.put('F', Character.toString('\u767C'));        
    }
    
      
    @Override
    public void paintComponent(Graphics g)
    {        
        super.paintComponent(g);
        String addSymbol = symbolMap.get(symbol);
        g.setColor(Color.RED);  
        g.drawString(Character.toString(symbol), 63, 15);
        font = g.getFont();   
        
                
        if(symbol == 'C')
        {
            font = font.deriveFont(font.getSize2D() * 3.5F);
            g.setFont(font);
            g.setColor(Color.RED);
            g.drawString(addSymbol, 25, 50);
        }
        else if(symbol == 'F')
        {
            font = font.deriveFont(font.getSize2D() * 3.5F);
            g.setFont(font);
            g.setColor(Color.GREEN);
            g.drawString(addSymbol, 25, 50);
        }
        else if(symbol == 'N' || symbol == 'E' || symbol == 'W' || symbol == 'S')
        {
            font = font.deriveFont(font.getSize2D() * 3.5F);
            g.setFont(font);
            g.setColor(Color.BLACK);
            g.drawString(addSymbol, 25, 50);           
        }
        else
        {
            font = font.deriveFont(font.getSize2D() * 1.8F);
            g.setFont(font);
            g.drawString("\u842C", 36, 55);
        
            
        
            g.setColor(Color.BLACK);
            g.drawString(addSymbol, 36, 30);
        }        
    }   
    
    
    @Override
    public boolean matches(Tile other)
    {
        if(!super.matches(other))
        {
            return false;
        }
        
        CharacterTile temp = (CharacterTile)other;        
        return this.symbol == temp.symbol;
    }
    
    @Override
    public String toString()
    {
        String symbolMeaning;
        
        switch(symbol)
        {
            case 'N':
                symbolMeaning = "North Wind";
                break;
                
            case 'E':
                symbolMeaning = "East Wind";
                break;
                
            case 'W':
                symbolMeaning = "West Wind";
                break;
            
            case 'S':
                symbolMeaning = "South Wind";
                break;
                
            case 'C':
                symbolMeaning = "Red Dragon";
                break;
                
            case 'F':
                symbolMeaning = "Green Dragon";
                break;
                
            default:
                symbolMeaning = "Character " + symbol;
        }
        return symbolMeaning;
    }
    
   /* public static void main(String[] args)
    {
        JFrame frame = new JFrame();
        JPanel tiles = new JPanel();
        JScrollPane scroller = new JScrollPane(tiles);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Character Tiles");
        frame.add(scroller);

        // Try something like this if your tiles don't fit on the screen.
        // Replace "tile width" and "tile height" with your values.
        //scroller.setPreferredSize(new Dimension(8 * tile width, 40 + tile height));

        tiles.add(new CharacterTile('1'));
        tiles.add(new CharacterTile('2'));
        tiles.add(new CharacterTile('3'));
        tiles.add(new CharacterTile('4'));
        tiles.add(new CharacterTile('5'));
        tiles.add(new CharacterTile('6'));
        tiles.add(new CharacterTile('7'));
        tiles.add(new CharacterTile('8'));
        tiles.add(new CharacterTile('9'));
        tiles.add(new CharacterTile('N'));
        tiles.add(new CharacterTile('E'));
        tiles.add(new CharacterTile('W'));
        tiles.add(new CharacterTile('S'));
        tiles.add(new CharacterTile('C'));
        tiles.add(new CharacterTile('F'));

        frame.pack();
        frame.setVisible(true);
    }*/
}
