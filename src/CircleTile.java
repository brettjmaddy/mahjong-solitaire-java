
import java.awt.*;
import javax.swing.JFrame;


public class CircleTile extends RankTile
{
    private Circle circle = new Circle();   
    private int rank;
    
    public CircleTile(int rank)
    {
        super(rank);
        this.rank = rank;
        
        setToolTipText(toString());
    }
    
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        circle.draw(g);
        
    }
    
    @Override    
    public String toString()
    {
        return "Circle " + super.rank;
    }
    
    private class Circle
    {
        private int x;
        private int y;
        private int d;

        private Circle()
        {
            
        }
        
        public void draw(Graphics g)
        {            
            switch(rank)
            {
                case 1:
                    d = 48;
                    x = 22;
                    y = 6;

                    Pancake p = new Pancake();
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x, y, d, d);
                    g.setColor(Color.RED);
                    g.fillOval(40, 24, 12, 12);
                    
                    p.drawPan(g);                   
                    break;

                case 2:
                    d = 24;
                    x = 34;
                    y = 6;

                    g.setColor(Color.GREEN);
                    g.fillOval(x, y, d, d);
                    g.setColor(Color.RED);
                    g.fillOval(x, y + d, d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 4), y + (d / 4), (x + d) - (d / 4), (y + d) - (d / 4));
                    g.drawLine(x + (d / 4), (y + d) - (d / 4), (x + d) - (d / 4), y + (d / 4));              
                    g.drawLine(x + (d / 4), (y + d) + (d / 4), (x + d) - (d / 4), (y + d + d) - (d / 4));
                    g.drawLine(x + (d / 4), (y + d + d) - (d / 4), (x + d) - (d / 4), (y + d) + (d / 4));
                    break;

                case 3:
                    d = 18;
                    x = 18;
                    y = 2;

                    g.setColor(Color.BLUE);
                    g.fillOval(x, y, d, d);

                    g.setColor(Color.RED);
                    g.fillOval(x + d, y + d, d, d);

                    g.setColor(Color.GREEN);
                    g.fillOval(x + d + d, y + d + d, d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 6), y + (d / 6), (x + d) - (d / 6), (y + d) - (d / 6));
                    g.drawLine(x + (d / 6), (y + d) - (d / 6), (x + d) - (d / 6), y + (d / 6));                
                    g.drawLine((x + d) + (d / 6), (y + d) + (d / 6), (x + d *2) - (d / 6), (y  + d *2) - (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + d *2) - (d / 6), (x + d *2) - (d / 6), (y + d) + (d / 6));                
                    g.drawLine((x + d *2) + (d / 6), (y + d *2) + (d / 6), (x + d *3) - (d / 6), (y  + d *3) - (d / 6));
                    g.drawLine((x + d *2) + (d / 6), (y + d *3) - (d / 6), (x + d *3) - (d / 6), (y + d *2) + (d / 6));
                    break;

                case 4:
                    d = 24;
                    x = 22;
                    y = 6;

                    g.setColor(Color.BLUE);
                    g.fillOval(22, 6, d, d);
                    g.fillOval(46, 30, d, d);

                    g.setColor(Color.GREEN);
                    g.fillOval(46, 6, d, d);
                    g.fillOval(22, 30, d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 4), y + (d / 4), (x + d) - (d / 4), (y + d) - (d / 4));
                    g.drawLine(x + (d / 4), (y + d) - (d / 4), (x + d) - (d / 4), y + ( d / 4));
                    g.drawLine(x + (d / 4), (y + d) + (d / 4), (x + d) - (d / 4), (y + d + d) - (d / 4));
                    g.drawLine(x + (d / 4), (y + d + d) - (d / 4), (x + d) - (d / 4), (y + d) + ( d / 4));                
                    g.drawLine((x + d) + (d / 4), y + (d / 4), (x + d + d) - (d / 4), (y + d) - (d / 4));
                    g.drawLine((x + d) + (d / 4), (y + d) - (d / 4), (x + d + d) - (d / 4), y + ( d / 4));
                    g.drawLine((x + d) + (d / 4), (y + d) + (d / 4), (x + d + d) - (d / 4), (y + d + d) - (d / 4));
                    g.drawLine((x + d) + (d / 4), (y + d + d) - (d / 4), (x + d + d) - (d / 4), (y + d) + ( d / 4));
                    break;

                case 5:
                    d = 18;
                    x = 18;
                    y = 2;

                    g.setColor(Color.BLUE);
                    g.fillOval(x, y, d, d);
                    g.fillOval(x + d *2, y + d *2, d, d);

                    g.setColor(Color.RED);
                    g.fillOval(x + d, y + d, d, d);               

                    g.setColor(Color.GREEN);
                    g.fillOval(x, y + d *2, d, d);
                    g.fillOval(x + d *2, y, d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 6), y + (d / 6), (x + d) - (d / 6), (y + d) - (d / 6));
                    g.drawLine(x + (d / 6), (y + d) - (d / 6), (x + d) - (d / 6), y + (d / 6));                
                    g.drawLine((x + d) + (d / 6), (y + d) + (d / 6), (x + d *2) - (d / 6), (y  + d *2) - (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + d *2) - (d / 6), (x + d *2) - (d / 6), (y + d) + (d / 6));                
                    g.drawLine((x + d *2) + (d / 6), (y + d *2) + (d / 6), (x + d *3) - (d / 6), (y  + d *3) - (d / 6));
                    g.drawLine((x + d *2) + (d / 6), (y + d *3) - (d / 6), (x + d *3) - (d / 6), (y + d *2) + (d / 6));                
                    g.drawLine(x + (d / 6), (y + d *2) + (d / 6), (x + d) - (d / 6), (y + d *3) - (d / 6));                
                    g.drawLine(x + (d / 6), (y + d *3) - (d / 6), (x + d) - (d / 6), (y + d *2) + (d / 6));                
                    g.drawLine((x + d *2) + (d / 6), y + (d / 6), (x + d *3) - (d / 6), (y + d) - (d / 6));
                    g.drawLine((x + d *2) + (d / 6), (y + d) - (d / 6), (x + d *3) - (d / 6), y + (d / 6));
                    break;

                case 6:
                   d = 18;
                   x = 25;
                   y = 2;

                   g.setColor(Color.GREEN);
                   g.fillOval(x, y, d, d);
                   g.fillOval((x *2), y, d, d);

                   g.setColor(Color.RED);
                   g.fillOval(x, y + d, d, d);
                   g.fillOval(x *2, y + d, d, d);               
                   g.fillOval(x, y + d *2, d, d);
                   g.fillOval(x *2, y + d *2, d, d);

                   g.setColor(Color.WHITE); 
                   g.drawLine(x + (d / 6), y + (d / 6), (x + d) - (d / 6), (y + d) - (d / 6));
                   g.drawLine(x + (d / 6), (y + d) - (d / 6), (x + d) - (d / 6), y + (d / 6));               
                   g.drawLine((x *2) + (d / 6), y + (d / 6), ((x *2) + d) - (d / 6), (y + d) - (d / 6));
                   g.drawLine((x *2) + (d / 6), (y + d) - (d / 6), ((x *2) + d) - (d / 6), y + (d / 6));               
                   g.drawLine(x + (d / 6), (y + d) + (d / 6), (x + d) - (d / 6), (y + d *2) - (d / 6));
                   g.drawLine(x + (d / 6), (y + d *2) - (d / 6), (x + d) - (d / 6), (y + d) + (d / 6));               
                   g.drawLine((x *2) + (d / 6), (y + d) + (d / 6), ((x *2) + d) - (d / 6), (y + d *2) - (d / 6));
                   g.drawLine((x *2) + (d / 6), (y + d *2) - (d / 6), ((x *2) + d) - (d / 6), (y + d) + (d / 6));               
                   g.drawLine(x + (d / 6), (y + d *2) + (d / 6), (x + d) - (d / 6), (y + d *3) - (d / 6));
                   g.drawLine(x + (d / 6), (y + d *3) - (d / 6), (x + d) - (d / 6), (y + d *2) + (d / 6));               
                   g.drawLine((x *2) + (d / 6), (y + d *2) + (d / 6), ((x *2) + d) - (d / 6), (y + d *3) - (d / 6));
                   g.drawLine((x *2) + (d / 6), (y + d *3) - (d / 6), ((x *2) + d) - (d / 6), (y + d *2) + (d / 6));
                   break;

                case 7:
                    d = 12;
                    x = 24;
                    y = 4;

                    g.setColor(Color.GREEN);
                    g.fillOval(x, y, d, d);
                    g.fillOval((x + d) + (d / 2), y + (d / 2), d, d);
                    g.fillOval((x + d *3), (y + d), d, d);

                    g.setColor(Color.RED);
                    g.fillOval(x + (d / 2), y + (d *2), d, d);
                    g.fillOval(x + (d *2), y + (d *2), d, d);                
                    g.fillOval(x + (d / 2), y + (d *3), d, d);
                    g.fillOval(x + (d *2), y + (d *3), d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 8), y + (d / 8), (x + d) - (d / 8), (y + d) - (d / 8));
                    g.drawLine(x + (d / 8), (y + d) - (d / 8), (x + d) - (d / 8), y + (d / 8));                
                    g.drawLine(((x + d) + (d / 2)) + (d / 8), (y + (d / 2)) + (d / 8), ((x + d) + (d / 2)) + d - (d / 8), (y + (d / 2) + d) - (d / 8));
                    g.drawLine(((x + d) + (d / 2)) + (d / 8), (y + (d / 2) + d) - (d / 8), ((x + d) + (d / 2)) + d - (d / 8), (y + (d / 2)) + (d / 8));                
                    g.drawLine((x + (d *3) + (d / 8)), (y + d) + (d / 8), (x + (d *4) - (d / 8)), (y + (d *2)) - (d / 8));
                    g.drawLine((x + (d *3) + (d / 8)), (y + (d *2)) - (d / 8), (x + (d *4) - (d / 8)), (y + d) + (d / 8));                
                    g.drawLine((x + (d / 2)) + (d / 8), (y + (d *2)) + (d / 8), (x + (d / 2)) + d - (d / 8), (y + (d *3)) - (d / 8));
                    g.drawLine((x + (d / 2)) + (d / 8), (y + (d *3)) - (d / 8), (x + (d / 2)) + d - (d / 8), (y + (d *2)) + (d / 8));                
                    g.drawLine((x + (d *2) + (d / 8)), (y + (d *2)) + (d / 8), (x + (d *3) - (d / 8)), (y + (d *3)) - (d / 8));
                    g.drawLine((x + (d *2) + (d / 8)), (y + (d *3)) - (d / 8), (x + (d *3) - (d / 8)), (y + (d *2)) + (d / 8));                
                    g.drawLine((x + (d / 2)) + (d / 8), (y + (d *3)) + (d / 8), (x + (d / 2)) + d - (d / 8), (y + (d *3)) + d - (d / 8));
                    g.drawLine((x + (d / 2)) + (d / 8), (y + (d *3)) + d - (d / 8), (x + (d / 2)) + d - (d / 8), (y + (d *3)) + (d / 8));                
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *3)) + (d / 8), (x + (d *2)) + d - (d / 8), (y + (d *4)) - (d / 8));
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *4)) - (d / 8), (x + (d *2)) + d - (d / 8), (y + (d *3)) + (d / 8));
                    break;

                case 8:
                    d = 12;
                    x = 28;
                    y = 6;

                    g.setColor(Color.BLUE);
                    g.fillOval(x, y, d, d);
                    g.fillOval(x + (d *2), y, d, d);
                    g.fillOval(x, y + d, d, d);
                    g.fillOval(x + (d *2), y +d, d, d);
                    g.fillOval(x, y + (d *2), d, d);
                    g.fillOval(x + (d *2), y + (d *2), d, d);
                    g.fillOval(x, y + (d *3), d, d);
                    g.fillOval(x + (d *2), y + (d *3), d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d /8), y + (d / 8), (x + d) - (d / 8), (y + d) - (d / 8));
                    g.drawLine(x + (d /8), (y + d) - (d / 8), (x + d) - (d / 8), y + (d / 8));                
                    g.drawLine(x + (d /8), (y + d) + (d / 8), (x + d) - (d / 8), (y + (d *2)) - (d / 8));
                    g.drawLine(x + (d /8), (y + (d * 2)) - (d / 8), (x + d) - (d / 8), (y + d) + (d / 8));                
                    g.drawLine(x + (d /8), (y + (d *2)) + (d / 8), (x + d) - (d / 8), (y + (d *3)) - (d / 8));
                    g.drawLine(x + (d /8), (y + (d * 3)) - (d / 8), (x + d) - (d / 8), (y + (d *2)) + (d / 8));                
                    g.drawLine(x + (d /8), (y + (d *3)) + (d / 8), (x + d) - (d / 8), (y + (d *4)) - (d / 8));
                    g.drawLine(x + (d /8), (y + (d * 4)) - (d / 8), (x + d) - (d / 8), (y + (d *3)) + (d / 8));                
                    g.drawLine((x + (d *2)) + (d / 8), y + (d / 8), (x + (d *3))  - (d / 8), (y + d) - (d / 8));
                    g.drawLine((x + (d *2)) + (d / 8), (y + d) - (d / 8), (x + (d *3))  - (d / 8), y + (d / 8));                
                    g.drawLine((x + (d *2)) + (d / 8), (y + d) + (d / 8), (x + (d *3))  - (d / 8), (y + (d *2)) - (d / 8));
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *2)) - (d / 8), (x + (d *3))  - (d / 8), (y + d) + (d / 8));                
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *2)) + (d / 8), (x + (d *3))  - (d / 8), (y + (d *3)) - (d / 8));
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *3)) - (d / 8), (x + (d *3))  - (d / 8), (y + (d *2)) + (d / 8));                
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *3)) + (d / 8), (x + (d *3))  - (d / 8), (y + (d *4)) - (d / 8));
                    g.drawLine((x + (d *2)) + (d / 8), (y + (d *4)) - (d / 8), (x + (d *3))  - (d / 8), (y + (d *3)) + (d / 8));
                    break;

                case 9:
                    d = 18;
                    x = 19;
                    y = 4;

                    g.setColor(Color.GREEN);
                    g.fillOval(x, y, d, d);
                    g.fillOval(x + d, y ,d, d);
                    g.fillOval(x + (d *2), y, d, d);

                    g.setColor(Color.RED);
                    g.fillOval(x, y + d, d, d);
                    g.fillOval(x + d, y + d, d, d);
                    g.fillOval(x + (d *2), y + d, d, d);                

                    g.setColor(Color.BLUE);
                    g.fillOval(x, y + (d *2), d, d);
                    g.fillOval(x + d, y + (d *2), d, d);
                    g.fillOval(x + (d *2), y + (d *2), d, d);

                    g.setColor(Color.WHITE);
                    g.drawLine(x + (d / 6), y + (d / 6), (x + d) - (d / 6), (y + d) - (d / 6));
                    g.drawLine(x + (d / 6), (y + d) - (d / 6), (x + d) - (d / 6), y + (d / 6));
                    g.drawLine((x + d) + (d / 6), y + (d / 6), (x + (d *2)) - (d / 6), (y + d) - (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + d ) - (d / 6), (x + (d *2)) - (d / 6), y + (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), y + (d / 6), (x + (d *3)) - (d / 6), (y + d) - (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), (y + d) - (d / 6), (x + (d *3)) - (d / 6), y + (d / 6));
                    g.drawLine(x + (d / 6), (y + d) + (d / 6), (x + d) - (d / 6), (y + (d *2)) - (d / 6));
                    g.drawLine(x + (d / 6), (y + (d *2)) - (d / 6), (x + d) - (d / 6), (y + d) + (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + d) + (d / 6), (x + (d *2)) - (d / 6), (y + (d *2)) - (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + (d *2)) - (d / 6), (x + (d *2)) - (d / 6), (y + d) + (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), (y + d) + (d / 6), (x + (d *3)) - (d / 6), (y + (d *2)) - (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), (y + (d *2)) - (d / 6), (x + (d *3)) - (d / 6), (y + d) + (d / 6));
                    g.drawLine(x + (d / 6), (y + (d *2)) + (d / 6), (x + d) - (d / 6), (y + (d *3)) - (d / 6));
                    g.drawLine(x + (d / 6), (y + (d *3)) - (d / 6), (x + d) - (d / 6), (y + (d *2)) + (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + (d *2)) + (d / 6), (x + (d *2)) - (d / 6), (y + (d *3)) - (d / 6));
                    g.drawLine((x + d) + (d / 6), (y + (d *3)) - (d / 6), (x + (d *2)) - (d / 6), (y + (d *2)) + (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), (y + (d *2)) + (d / 6), (x + (d *3)) - (d / 6), (y + (d *3)) - (d / 6));
                    g.drawLine((x + (d *2)) + (d / 6), (y + (d *3)) - (d / 6), (x + (d *3)) - (d / 6), (y + (d *2)) + (d / 6));
                    break;
            }
        }
        
        
    }
    
    private class Pancake
        {
            private Pancake()
            {                
            }
            
            private void drawPan(Graphics g)
            {
                g.setColor(Color.WHITE);
                g.drawLine(42, 26, 51, 35);
                g.drawLine(42, 35, 51, 26);
                
                g.fillOval(42, 8, 8, 8);
                g.fillOval(42, 44, 8, 8);
                g.fillOval(24, 26, 8, 8);
                g.fillOval(60, 26, 8, 8);
                g.fillOval(54, 38, 8, 8);
                g.fillOval(30, 38, 8, 8);
                g.fillOval(30, 14, 8, 8);
                g.fillOval(54, 14, 8, 8);
            }
        }
    
        
    /*public static void main(String[] args)
	{
		JFrame	frame = new JFrame();

		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setTitle("Circle Tiles");

		frame.add(new CircleTile(1));
		frame.add(new CircleTile(2));
		frame.add(new CircleTile(3));
		frame.add(new CircleTile(4));
		frame.add(new CircleTile(5));
		frame.add(new CircleTile(6));
		frame.add(new CircleTile(7));
		frame.add(new CircleTile(8));
		frame.add(new CircleTile(9));

		frame.pack();
		frame.setVisible(true);
	}*/
    
    
}
