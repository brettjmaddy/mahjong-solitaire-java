import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class MahJong extends JFrame
{  
    MahJongBoard board;
    private String rules = "Select two matching tiles that are free\n"
            + "to remove. To determine if a tile is free to remove it \n"
            + " must not have tiles above it, or tiles on its right and \n"
            + " left sides. As long as one of its sides does not have a \n"
            + " tile touching it, and no tiles above it, it is free to \n"
            + "be removed. Season tiles, which are the snowflake, sun, \n"
            + "leaf, and butterfly tiles, can be matched with each other. \n"
            + "Flower tiles, which are the plum, bamboo shute, orchid,  \n"
            + "and chrysanthemum tiles, may also be matched with each other. \n"
            + "Every other tile must be matched with its identical equal.";
    private String operation = "-Selecting a free tile will make it become highlighted.\n\n"
            + "-If you selected a tile that is not free, then it will not become\n"
            + " highlighted. This idicates that you have selected an invalid tile\n"
            + " to remove. Similarly, if the second tile you select is not a match\n"
            + " to the first, highlighted tile, or the second tile is not free, the\n"
            + " highlight will go away on the first tile.\n\n"
            + "-The Play menu allows the player to play a new game, restart their\n"
            + " current game, or play a previous game based on game number.\n\n"
            + "-The Sound menu allows the player to toggle the game sound.\n\n"
            + "-The Move menu allows the user to undo a move.";
    
    Fireworks fw = new Fireworks();
            
    
    
    
    public MahJong()
    {    
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setBackground(Color.YELLOW);
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter()
        {
            @Override
            public void windowClosing(WindowEvent we)
            {
                exit();
            }
        });

        add(board = new MahJongBoard());
        setSize(1200, 800);
        
        setTitle("MahJong Solitaire      " + "Game Number: " + board.gameNumber);
        
        makeMenu();

        setVisible(true);       
    }
    
    private void exit()
    {
        if(JOptionPane.showConfirmDialog(this, "Do you wish to exit the game?", "Exit the Game", 
                                                JOptionPane.YES_NO_OPTION,
                                                JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
        {
            System.exit(0);
        }
    }  
    
    private void makeMenu()
    {
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        
        JMenu game = new JMenu("Game");
        game.setMnemonic('G');
        menuBar.add(game);
        
        JMenuItem play = new JMenuItem("Play", 'P');
        play.setToolTipText("Start a new game");
        play.setAccelerator(KeyStroke.getKeyStroke("ctrl P"));        
        game.add(play);
        
        play.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent ae)
            {
                newGame();
            }
        });
        
        JMenuItem restart = new JMenuItem("Restart", 'R');
        restart.setToolTipText("Restart current game");
        restart.setAccelerator(KeyStroke.getKeyStroke("ctrl R"));
        game.add(restart);
        
        restart.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent ae)
            {
                restart();
            }
        });
        
        JMenuItem numbered = new JMenuItem("Numbered", 'N');
        numbered.setToolTipText("Play a numbered game");
        numbered.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
        game.add(numbered);
        
        numbered.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent ae)
            {
                getNumberedGame();
            }
        });
        
        JMenuItem exit = new JMenuItem("Exit", 'E');
        exit.setToolTipText("Exit the game");
        exit.setAccelerator(KeyStroke.getKeyStroke("ctrl E"));
        game.add(exit);
        
        exit.addActionListener(new ActionListener()
        {
            @Override public void actionPerformed(ActionEvent ae)
            {
                exit();
            }
        });
        
        JMenu sound = new JMenu("Sound");
        sound.setMnemonic('S');
        menuBar.add(sound);
        
        ButtonGroup group = new ButtonGroup();
        
        JRadioButtonMenuItem on = new JRadioButtonMenuItem("On", true);
        on.setToolTipText("Sound on");
        on.setAccelerator(KeyStroke.getKeyStroke("ctrl D"));
        on.setMnemonic('D');
        group.add(on);
        sound.add(on);
        
        on.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                soundOn();
            }
        });
        
        JRadioButtonMenuItem off = new JRadioButtonMenuItem("Off");
        off.setToolTipText("Sound off");
        off.setAccelerator(KeyStroke.getKeyStroke("ctrl X"));
        off.setMnemonic('X');
        group.add(off);
        sound.add(off);
        
        off.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                soundOff();
            }
        });
        
        JMenu move = new JMenu("Move");
        move.setMnemonic('M');
        menuBar.add(move);        
        
        JMenuItem undo = new JMenuItem("Undo", 'U');
        undo.setToolTipText("Undo move");
        undo.setAccelerator(KeyStroke.getKeyStroke("ctrl U"));
        move.add(undo);
        
        undo.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                undo();
            }
        });
        
        JMenu help = new JMenu("Help");
        help.setMnemonic('H');
        menuBar.add(help);
        
        JMenuItem operation = new JMenuItem("Operation", 'O');
        operation.setToolTipText("Game operation");
        operation.setAccelerator(KeyStroke.getKeyStroke("ctrl O"));
        help.add(operation);
        
        operation.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                operations();
            }
        });
        
        JMenuItem rules = new JMenuItem("Game Rules", 'L');
        rules.setToolTipText("Rules of the game");
        rules.setAccelerator(KeyStroke.getKeyStroke("ctrl L"));
        help.add(rules);
        
        rules.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent ae)
            {
                gameRules();
            }
        });
    }
    
    private void newGame()
    {
        if(JOptionPane.showConfirmDialog(this, "Do you wish to end this game and start a new one?",
                                     "Start new game", JOptionPane.YES_NO_OPTION, 
                                     JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
        {            
            dispose();
            board.gameNumber = 0;
            new MahJong();
        }                
    }
    
    private void restart()
    {
        if(JOptionPane.showConfirmDialog(this, "Do you wish to restart this game?",
                                     "Restart Game", JOptionPane.YES_NO_OPTION, 
                                     JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION)
        {            
            dispose();
            new MahJong();
        }
    }
    
    private void getNumberedGame()
    {
        String selection = JOptionPane.showInputDialog(this, "Please enter the 5 digit game number",
                                    "Numbered Game", JOptionPane.INFORMATION_MESSAGE);
        
        if(selection != null)
        {            
            dispose();
            MahJongBoard.gameNumber = Integer.parseInt(selection);
            new MahJong();
        }
        else
        {            
            dispose();
            MahJongBoard.gameNumber = 0;
            new MahJong();
            
        }        
    }
            
    private void gameRules()
    {
        JOptionPane.showMessageDialog(this, rules, "Games Rules", JOptionPane.CLOSED_OPTION);
    }
    
    private void soundOn()
    {
        board.setGameSound(true);
    }
    
    private void soundOff()
    {
        board.setGameSound(false);
    }
    
    private void undo()
    {
        JOptionPane.showConfirmDialog(this, "This operation is not currently supported in this version",
                                        "Undo", JOptionPane.CLOSED_OPTION);
    }
        
    private void operations()
    {
        
            JOptionPane.showMessageDialog(this, operation, "Operations", JOptionPane.CLOSED_OPTION);
        
    }
       
    public static void main(String[] args)
    {
        new MahJong();
    }
}
