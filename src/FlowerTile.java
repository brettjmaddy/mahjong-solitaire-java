import java.awt.*;
import javax.swing.*;

public class FlowerTile extends PictureTile
{
    private String name;
    private ImageIcon icon;
    private Image image;
    private Image newimg;
    public FlowerTile(String name)
    {
        super(name);
        this.name = name;
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        switch(name)
        {
            case "Chrysanthemum":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Chrysanthemum.png"));
                
                image = icon.getImage(); 
                newimg = image.getScaledInstance(52, -1,  java.awt.Image.SCALE_SMOOTH); 
                icon = new ImageIcon(newimg); 
                break;
                
            case "Orchid":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Orchid.png"));
                
                image = icon.getImage(); 
                newimg = image.getScaledInstance(45, -1,  java.awt.Image.SCALE_SMOOTH); 
                icon = new ImageIcon(newimg); 
                break;
                
            case "Plum":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Plum.png"));
                
                image = icon.getImage(); 
                newimg = image.getScaledInstance(40, -1,  java.awt.Image.SCALE_SMOOTH); 
                icon = new ImageIcon(newimg); 
                break;
                
            case "Bamboo":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Bamboo.png"));
                
                image = icon.getImage(); 
                newimg = image.getScaledInstance(44, -1,  java.awt.Image.SCALE_SMOOTH); 
                icon = new ImageIcon(newimg); 
                break;
        }
        
         
        icon.paintIcon(this, g, 22, 6);
    }
    
    /*public static void main(String[] args)
    {
        JFrame frame = new JFrame();

        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Flower Tiles");

        frame.add(new FlowerTile("Chrysanthemum"));
        frame.add(new FlowerTile("Orchid"));
        frame.add(new FlowerTile("Plum"));
        frame.add(new FlowerTile("Bamboo"));

        frame.pack();
        frame.setVisible(true);
    }*/
}
