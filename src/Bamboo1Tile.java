import java.awt.*;
import javax.swing.*;

public class Bamboo1Tile extends PictureTile
{
    private ImageIcon icon;
    
    public Bamboo1Tile() 
    {
        super("Bamboo 1");
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Sparrow.png"));
        
        Image image = icon.getImage(); 
        Image newimg = image.getScaledInstance(50, -1,  java.awt.Image.SCALE_SMOOTH); 
        icon = new ImageIcon(newimg);  
        icon.paintIcon(this, g, 22, 6);
        
    }
        
    public String toString()
    {
        return super.toString();
    }
    
    /*public static void main(String[] args)
    {
        JFrame frame = new JFrame();

        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Bamboo 1 Tile");

        frame.add(new Bamboo1Tile());

        frame.pack();
        frame.setVisible(true);
    }*/
}
