import java.util.ArrayList;


public class MahJongModel 
{    
    final int row = 8;
    final int col = 12;
    final int layer = 4;    
    
    
    Tile[][][] model = new Tile[row][col][layer];
    Tile[] specialCases = new Tile[4];
    
    
    public MahJongModel()
    {
        
    }
    
    public void createStructure(ArrayList<Tile> deck)
    {     
        specialCases[0] = deck.remove(deck.size() - 1);
        specialCases[1] = deck.remove(deck.size() - 1);
        specialCases[2] = deck.remove(deck.size() - 1);
        specialCases[3] = deck.remove(deck.size() - 1);
        
        for(Tile t : deck)
        {
            model[t.getRow()][t.getCol()][t.getLayer()] = t;
            t.remove(t);
        }
        
        //System.out.println(specialCases[2]);
        
        
    }
    
    public int calcXCoords(int c, int l)
    {
        return (c * 59) + (l * 60);        
    }
    
    public int calcYCoords(int r, int l)
    {
        return (r * 59) + (-l * 60);
    }
    
    public boolean isTileOpen(int col, int row, int layer)
    {  
        int colDim = 11;        
                
        if(col == 99)
        {
            return specialCases[3] == null;
        }
        
        if(layer == 3)
        {
            return specialCases[0] == null;
        }
        
        //left side bottom layer
        if(col - 1 < 0 && row != 3 && row != 4)
        {
            return true;
        }        
        if(col - 1 < 0 && (row == 3 || row == 4))
        {
            return specialCases[1] == null;
        }        
        
        //right side bottom layer        
        if(col + 1 > colDim && (row != 3 || row != 4))
        {
            return true;
        }        
        if(col + 1 > colDim && (row == 3 || row == 4))
        {
            return specialCases[2] == null;
        }
        
        return model[row][col][layer + 1] == null && (model[row][col + 1][layer] == null || model[row][col - 1][layer] ==null);
                
    }
    
    public void removeTile(Tile t)
    {        
        if(t.getLayer() == 4)
        {
            specialCases[0] = null;
        }
        else if(t.getRow() == 90)
        {
            specialCases[1] = null;
        }
        else if(t.getCol() == 99)
        {
            specialCases[2] = null;
        }
        else if(t.getCol() == 100)
        {
            specialCases[3] = null;
        }
        else
        {
            model[t.getRow()][t.getCol()][t.getLayer()] = null;
        }       
        
    }
    
        
    
    
}
