
import java.awt.*;


public class RankTile extends Tile
{
    protected int rank;
    
    public RankTile(int rank)
    {
        this.rank = rank;
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);        
    }
    
    @Override
    public boolean matches(Tile other)
    {
        if(!super.matches(other))
        {
            return false;
        }
        
        RankTile temp = (RankTile)other;
        return this.rank == temp.rank;
    }
}
