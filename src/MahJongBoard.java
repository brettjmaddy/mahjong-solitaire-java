import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import javax.swing.*;

public class MahJongBoard extends JPanel implements MouseListener
{    
    private int x;
    private int y;
    final int padding = 180;
    static long gameNumber;
    
    private ArrayList<Tile> tempDeck;
    private ArrayList<Tile> positionDeck;
    private ArrayList<Tile> modelDeck;
       
    Tile t;
    Tile specialT1;
    Tile specialT2;
    Tile specialT3;
    Tile specialT4;
    
    PlayClip clip = new PlayClip("resources/audio/stone-scraping.wav", true); 
    Fireworks fw = new Fireworks(this);
    boolean gameSound = true;
    
    private MahJongModel model;   
    
    Tile[] selected = new Tile[2];    
    
    public MahJongBoard()
    {           
        setLayout(null);
        setOpaque(false);
        
        tempDeck = new ArrayList<>();        
        positionDeck = new ArrayList<>();
        modelDeck = new ArrayList<>();
        
        fillTempDeck();
        
        setTilePositions();
        
        tempDeck.clear();        
        tempDeck = null;
        
        fillmodelDeck();
        
        setTileListeners();
                
        
        model = new MahJongModel();

        model.createStructure(modelDeck);
        modelDeck.clear();
        modelDeck = null;
        
        drawGameBoard();
        
        positionDeck.clear();
        positionDeck = null;
        
        
    }
    
    private void fillTempDeck()
    {
        for(int i = 0; i < 4; i++)
        {
            tempDeck.add(new CharacterTile('1'));
            tempDeck.add(new CharacterTile('2'));
            tempDeck.add(new CharacterTile('3'));
            tempDeck.add(new CharacterTile('4'));
            tempDeck.add(new CharacterTile('5'));
            tempDeck.add(new CharacterTile('6'));
            tempDeck.add(new CharacterTile('7'));
            tempDeck.add(new CharacterTile('8'));
            tempDeck.add(new CharacterTile('9'));
            tempDeck.add(new CharacterTile('N'));
            tempDeck.add(new CharacterTile('E'));
            tempDeck.add(new CharacterTile('W'));            
            tempDeck.add(new CharacterTile('S'));
            tempDeck.add(new CharacterTile('C'));
            tempDeck.add(new CharacterTile('F'));
            tempDeck.add(new CircleTile(1));
            tempDeck.add(new CircleTile(2));
            tempDeck.add(new CircleTile(3));
            tempDeck.add(new CircleTile(4));
            tempDeck.add(new CircleTile(5));
            tempDeck.add(new CircleTile(6));
            tempDeck.add(new CircleTile(7));
            tempDeck.add(new CircleTile(8));
            tempDeck.add(new CircleTile(9));
            tempDeck.add(new BambooTile(2));
            tempDeck.add(new BambooTile(3));
            tempDeck.add(new BambooTile(4));
            tempDeck.add(new BambooTile(5));
            tempDeck.add(new BambooTile(6));
            tempDeck.add(new BambooTile(7));
            tempDeck.add(new BambooTile(8));
            tempDeck.add(new BambooTile(9));
            tempDeck.add(new WhiteDragonTile());
            tempDeck.add(new Bamboo1Tile());  
        }    
            
        tempDeck.add(new FlowerTile("Chrysanthemum"));
        tempDeck.add(new FlowerTile("Orchid"));
        tempDeck.add(new FlowerTile("Plum"));
        tempDeck.add(new FlowerTile("Bamboo"));
        tempDeck.add(new SeasonTile("Spring"));
        tempDeck.add(new SeasonTile("Summer"));
        tempDeck.add(new SeasonTile("Fall"));
        tempDeck.add(new SeasonTile("Winter"));
        
        if(gameNumber == 0)
        {
            gameNumber = System.currentTimeMillis() % 100000;
        }

        Collections.shuffle(tempDeck, new Random(gameNumber));
        Collections.shuffle(tempDeck, new Random(gameNumber));
        
        selected[0] = null;
        selected[1] = null;    
        
    }
    
    
    
    
        
    private void setTilePositions()
    {            
        int col;
        for(col = 11; col >= 0; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(0);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
            
        }


        for(col = 9; col >= 2; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(1);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));                
        }


        for(col = 10; col >= 1; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(2);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));                
        }


        for(col = 11; col >= 0; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(3);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 11; col >= 0; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(4);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 10; col >= 1; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(5);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 9; col >= 2; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(6);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 11; col >= 0; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(7);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(0);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(1);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(2);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(3);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(4);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(5);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 8; col >= 3; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(6);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(1);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 7; col >= 4; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(2);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(2);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 7; col >= 4; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(3);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(2);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 7; col >= 4; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(4);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(2);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 7; col >= 4; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(5);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(2);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 6; col >= 5; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(3);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(3);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        }

        for(col = 6; col >= 5; col--)
        {
            tempDeck.get(tempDeck.size() - 1).setRow(4);
            tempDeck.get(tempDeck.size() - 1).setCol(col);
            tempDeck.get(tempDeck.size() - 1).setLayer(3);                
            positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        } 
        
        tempDeck.get(tempDeck.size() - 1).setRow(99);
        tempDeck.get(tempDeck.size() - 1).setCol(100);
        tempDeck.get(tempDeck.size() - 1).setLayer(0);
        positionDeck.add(tempDeck.remove(tempDeck.size() - 1));       
        
        tempDeck.get(tempDeck.size() - 1).setRow(99);
        tempDeck.get(tempDeck.size() - 1).setCol(99);
        tempDeck.get(tempDeck.size() - 1).setLayer(0);
        positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        
        tempDeck.get(tempDeck.size() - 1).setRow(90);
        tempDeck.get(tempDeck.size() - 1).setCol(0);
        tempDeck.get(tempDeck.size() - 1).setLayer(0);
        positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
        
        tempDeck.get(tempDeck.size() - 1).setRow(999);
        tempDeck.get(tempDeck.size() - 1).setCol(999);
        tempDeck.get(tempDeck.size() - 1).setLayer(4);
        positionDeck.add(tempDeck.remove(tempDeck.size() - 1));
    }
    
    private void fillmodelDeck()
    {
        for(Tile t : positionDeck)
        {
            modelDeck.add(t);
        }
    }
                    
    private void drawGameBoard()
    {
        specialT1 = positionDeck.remove(positionDeck.size() - 1);
        specialT1.setLocation(615, 279);
        add(specialT1);
        specialT2 = positionDeck.remove(positionDeck.size() - 1);
        specialT3 = positionDeck.remove(positionDeck.size() - 1);
        specialT4 = positionDeck.remove(positionDeck.size() - 1);
        
        int count = 0;
        while(count < 4)
        {
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x - 86, padding + y + 88);
            add(t);
            count++;
        }
        
        count = 0;
        while(count < 16)
        {
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x - 43, padding + y + 44);
            add(t);  
            count ++;
        }
        count = 0;
        while(count < 36)
        {
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x, padding + y);
            add(t);  
            count ++;
        }
        
        count = 0;
        while(count < 30)
        {          
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x + 43, padding + y - 44);
            add(t);
            count++;
        }
        
        specialT2.setLocation(164, 342);        
        add(specialT2);
        
        count = 0;
        while(count < 26)
        {
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x + 43, padding + y - 44);
            add(t);
            count++;
        }
        
        specialT3.setLocation(931, 342);
        add(specialT3);
        
        specialT4.setLocation(990, 342);
        add(specialT4);
        
        while(!positionDeck.isEmpty())
        {
            t = positionDeck.remove(positionDeck.size() - 1);
            x = model.calcXCoords((t.getCol()), t.getLayer());
            y = model.calcYCoords((t.getRow()), t.getLayer());
            t.setLocation(padding + x + 43, padding + y - 44);
            add(t);
            count++;
        }
    }
    
    private void setTileListeners()
    {
        for(Tile tile : positionDeck)
        {
            tile.addMouseListener(this);
        }
    }

    @Override
    public void mouseClicked(MouseEvent me) 
    {       
        Tile t = (Tile)me.getSource();
                       
        if(model.isTileOpen(t.getCol(), t.getRow(), t.getLayer()))
        {
            if(selected[0] == null)
            {                
                selected[0] = t;
                selected[0].isSelected = true;
                repaint();                
            }
            else
            {                
                selected[1] = t;
                
                selected[0].isSelected = false;
                repaint();
                
                if(selected[0].matches(selected[1]))
                {
                    remove(selected[0]);
                    model.removeTile(selected[0]);
                    remove(selected[1]);
                    model.removeTile(selected[1]);                   
                    
                    if(gameSound)
                    {
                        clip.play();
                    }
                    
                    repaint();
                                                   
                    if(getComponentCount() == 0)
                    {   
                        fw.setExplosions(0, 1000);
                        fw.fire();
                    }              
                    
                }
                
                selected[0] = null;
                selected[1] = null;                
            }           
        }
        else
        {
            if(selected[0] != null)
            {
                selected[0].isSelected = false;
                repaint();
                
                selected[0] = null;
                selected[1] = null;
            }
        }
    }
            
    public void setGameSound(boolean sound)
    {
        gameSound = sound;
        fw.setSound(sound);       
    }

    @Override
    public void mousePressed(MouseEvent me) {}

    @Override
    public void mouseReleased(MouseEvent me) {}

    @Override
    public void mouseEntered(MouseEvent me) {}

    @Override
    public void mouseExited(MouseEvent me) {}
}
    
    
    

