import java.awt.*;


public class WhiteDragonTile extends Tile
{
    private int x;
    private int y;
    private int w;
    private int l;
    public WhiteDragonTile()
    {
        setToolTipText(toString());
    }
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        x = 20;
        y = 4;
        w = 7;
        l = 11;
        
        g.drawRect(x, y, 51, 52);
        g.drawRect(x + 6, y + 6, 39, 40);
        
        g.setColor(Color.BLUE);
        g.fillRect(x, y, w, l);
        g.fillRect(x, y + (l *2) - 1, w, l);
        g.fillRect(x, y + (l *4) - 2, w, l);
        g.fillRect(x + (l *2) - 1, y, l, w);
        g.fillRect(x + 45, y, w, l);
        g.fillRect(x + 45, y + (l *2) -1, w, l);
        g.fillRect(x + 45, y + (l *4) -2, w, l);
        g.fillRect(x + (l *2) -1, y + 46, l, w);
      
        
        
    }
    public String toString()
    {
        return "White Dragon";
    }
}
