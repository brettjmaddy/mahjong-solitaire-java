import java.awt.*;
import javax.swing.*;

public class BambooTile extends RankTile
{
    private int rank;
    private Bamboo bamboo = new Bamboo();
    public BambooTile(int rank)
    {
        super(rank);
        this.rank = rank;
        
        setToolTipText(toString());
    }
    
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        bamboo.draw(g);
        
    }
    
    @Override
    public String toString()
    {
        return "Bamboo " + super.rank;        
    }
    
    private class Bamboo
    {
        private int x;
        private int y;
        private int rw = 8;
        private int rh = 6;
        private int bw = 4;
        private int bh = 8;
        private int th = 16;
        
        private Bamboo()
        {            
        }
        
        private void draw(Graphics g)
        {
            switch(rank)
            {
                case 2:
                    x = 43;
                    y = 10; 
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x, y, rw, rh);
                    g.fillRect(x + (bw / 2), y + 6, bw, th);
                    g.fillOval(x, y + 8, rw, rh);
                    g.fillOval(x, y + 16, rw, rh);                    
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x, 34, rw, rh);
                    g.fillRect(x + (bw / 2), 36, bw, th);
                    g.fillOval(x, 42, rw, rh);
                    g.fillOval(x, 50, rw, rh);
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(47, 6, 47, 38);
                    g.drawLine(47, 34, 47, 54);
                    
                    break;
                    
                case 3:
                    x = 43;
                    y = 10; 
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x, y, rw, rh);
                    g.fillRect(x + (bw / 2), y + 6, bw, th);
                    g.fillOval(x, y + 8, rw, rh);
                    g.fillOval(x, y + 16, rw, rh);                    
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, 34, rw, rh);
                    g.fillRect((x -16) + (bw / 2), 36, bw, th);
                    g.fillOval(x - 16, 42, rw, rh);
                    g.fillOval(x - 16, 50, rw, rh);
                    g.fillOval(x + 16, 34, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), 36, bw, th);
                    g.fillOval(x + 16, 42, rw, rh);
                    g.fillOval(x + 16, 50, rw, rh);
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(47, 6, 47, 38);
                    g.drawLine(31, 34, 31, 54);
                    g.drawLine(63, 34, 63, 54);
                    break;
                    
                case 4:
                    x = 43;
                    y = 10;
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x - 16, y, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x - 16, y + 8, rw, rh);
                    g.fillOval(x - 16, y + 16, rw, rh);
                    g.fillOval(x + 16, 34, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), 36, bw, th);
                    g.fillOval(x + 16, 42, rw, rh);
                    g.fillOval(x + 16, 50, rw, rh);
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, 34, rw, rh);
                    g.fillRect((x -16) + (bw / 2), 36, bw, th);
                    g.fillOval(x - 16, 42, rw, rh);
                    g.fillOval(x - 16, 50, rw, rh);
                    g.fillOval(x + 16, y, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x + 16, y + 8, rw, rh);
                    g.fillOval(x + 16, y + 16, rw, rh);
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(31, 6, 31, 38);
                    g.drawLine(63, 6, 63, 38);
                    g.drawLine(31, 34, 31, 54);
                    g.drawLine(63, 34, 63, 54);
                    
                    break;
                    
                case 5:
                    x = 43;
                    y = 10;
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, y, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x - 16, y + 8, rw, rh);
                    g.fillOval(x - 16, y + 16, rw, rh);
                    g.fillOval(x + 16, 34, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), 36, bw, th);
                    g.fillOval(x + 16, 42, rw, rh);
                    g.fillOval(x + 16, 50, rw, rh);
                                        
                    g.setColor(Color.BLUE);
                    g.fillOval(x - 16, 34, rw, rh);
                    g.fillRect((x -16) + (bw / 2), 36, bw, th);
                    g.fillOval(x - 16, 42, rw, rh);
                    g.fillOval(x - 16, 50, rw, rh);
                    g.fillOval(x + 16, y, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x + 16, y + 8, rw, rh);
                    g.fillOval(x + 16, y + 16, rw, rh);
                    
                    g.setColor(Color.RED);
                    g.fillOval(x, 20, rw, rh);
                    g.fillRect(x + (bw / 2), y + 16, bw, th);
                    g.fillOval(x, y + 18, rw, rh);
                    g.fillOval(x, y + 26, rw, rh);                    
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(31, 6, 31, 38);
                    g.drawLine(63, 6, 63, 38);
                    g.drawLine(31, 34, 31, 54);
                    g.drawLine(63, 34, 63, 54);
                    g.drawLine(47, 16, 47, 48);
                    break;
                    
                case 6:
                    x = 43;
                    y = 10;
                                        
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, y, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x - 16, y + 8, rw, rh);
                    g.fillOval(x - 16, y + 16, rw, rh);
                    g.fillOval(x, y, rw, rh);
                    g.fillRect(x + (bw / 2), y + 6, bw, th);
                    g.fillOval(x, y + 8, rw, rh);
                    g.fillOval(x, y + 16, rw, rh);
                    g.fillOval(x + 16, y, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 6, bw, th);
                    g.fillOval(x + 16, y + 8, rw, rh);
                    g.fillOval(x + 16, y + 16, rw, rh);                    
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x, 34, rw, rh);
                    g.fillRect(x + (bw / 2), 36, bw, th);
                    g.fillOval(x, 42, rw, rh);
                    g.fillOval(x, 50, rw, rh);
                    g.fillOval(x + 16, 34, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), 36, bw, th);
                    g.fillOval(x + 16, 42, rw, rh);
                    g.fillOval(x + 16, 50, rw, rh);
                    g.fillOval(x - 16, 34, rw, rh);
                    g.fillRect((x -16) + (bw / 2), 36, bw, th);
                    g.fillOval(x - 16, 42, rw, rh);
                    g.fillOval(x - 16, 50, rw, rh);
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(47, 6, 47, 38);
                    g.drawLine(47, 34, 47, 54);
                    g.drawLine(31, 6, 31, 38);
                    g.drawLine(63, 6, 63, 38);
                    g.drawLine(31, 34, 31, 54);
                    g.drawLine(63, 34, 63, 54);
                    break;
                    
                case 7:
                    x = 43;
                    y = 10;
                    rw = 8;
                    rh = 4;
                    bw = 4;
                    bh = 7;
                    th = 15;                    
                    
                    g.setColor(Color.RED);
                    g.fillOval(x, 1, rw, rh);
                    g.fillRect(x + (bw / 2), y - 6, bw, th);
                    g.fillOval(x, y - 3, rw, rh);
                    g.fillOval(x, y + 5, rw, rh);                    
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x, 20, rw, rh);
                    g.fillRect(x + (bw / 2), y + 14, bw, th);
                    g.fillOval(x, y + 18, rw, rh);
                    g.fillOval(x, y + 24, rw, rh);
                    g.fillOval(x, 39, rw, rh);
                    g.fillRect(x + (bw / 2), y + 33, bw, th);
                    g.fillOval(x, y + 36, rw, rh);
                    g.fillOval(x, y + 45, rw, rh);
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, 39, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x - 16, y + 36, rw, rh);
                    g.fillOval(x - 16, y + 45, rw, rh);
                    g.fillOval(x + 16, 39, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x + 16, y + 36, rw, rh);
                    g.fillOval(x + 16, y + 45, rw, rh);
                    g.fillOval(x - 16, 20, rw, rh);
                    g.fillRect((x -16) + (bw / 2), 20, bw, th);
                    g.fillOval(x - 16, 26, rw, rh);
                    g.fillOval(x - 16, 34, rw, rh);
                    g.fillOval(x + 16, 20, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), 20, bw, th);
                    g.fillOval(x + 16, 26, rw, rh);
                    g.fillOval(x + 16, 34, rw, rh);
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(47, 3, 47, 29);  
                    g.drawLine(47, 16, 47, 38);
                    g.drawLine(47, 35, 47, 57);
                    g.drawLine(31, 35, 31, 57);
                    g.drawLine(63, 35, 63, 57);
                    g.drawLine(31, 14, 31, 38);
                    g.drawLine(63, 14, 63, 38);
                    break;                    
                  
                case 8:
                    x = 43;
                    y = 10;
                    rw = 8;
                    rh = 4;
                    bw = 4;
                    bh = 7;
                    th = 15;
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, 1, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y - 6, bw, th);
                    g.fillOval(x - 16, y - 3, rw, rh);
                    g.fillOval(x - 16, y + 5, rw, rh);
                    g.fillOval(x, 1, rw, rh);
                    g.fillRect(x + (bw / 2), y - 6, bw, th);
                    g.fillOval(x, y - 3, rw, rh);
                    g.fillOval(x, y + 5, rw, rh);
                    g.fillOval(x + 16, 1, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y - 6, bw, th);
                    g.fillOval(x + 16, y - 3, rw, rh);
                    g.fillOval(x + 16, y + 5, rw, rh);
                    
                    g.setColor(Color.RED);
                    g.fillOval(x - 10, 20, rw, rh);
                    g.fillRect((x - 10) + (bw / 2), y + 14, bw, th);
                    g.fillOval(x - 10, y + 18, rw, rh);
                    g.fillOval(x - 10, y + 24, rw, rh);
                    g.fillOval(x + 10, 20, rw, rh);
                    g.fillRect((x + 10) + (bw / 2), y + 14, bw, th);
                    g.fillOval(x + 10, y + 18, rw, rh);
                    g.fillOval(x + 10, y + 24, rw, rh);
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x - 16, 39, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x - 16, y + 36, rw, rh);
                    g.fillOval(x - 16, y + 45, rw, rh);
                    g.fillOval(x, 39, rw, rh);
                    g.fillRect(x + (bw / 2), y + 33, bw, th);
                    g.fillOval(x, y + 36, rw, rh);
                    g.fillOval(x, y + 45, rw, rh);
                    g.fillOval(x + 16, 39, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x + 16, y + 36, rw, rh);
                    g.fillOval(x + 16, y + 45, rw, rh);                    
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(31, 3, 31, 27);
                    g.drawLine(47, 3, 47, 27);
                    g.drawLine(63, 3, 63, 27);
                    g.drawLine(37, 16, 37, 36);
                    g.drawLine(57, 16, 57, 36);
                    g.drawLine(31, 35, 31, 57);
                    g.drawLine(47, 35, 47, 57);
                    g.drawLine(63, 35, 63, 57);                        
                    break;
                    
                case 9:
                    x = 43;
                    y = 10;
                    rw = 8;
                    rh = 4;
                    bw = 4;
                    bh = 7;
                    th = 15;
                    
                    g.setColor(Color.GREEN);
                    g.fillOval(x - 16, 1, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y - 6, bw, th);
                    g.fillOval(x - 16, y - 3, rw, rh);
                    g.fillOval(x - 16, y + 5, rw, rh);
                    g.fillOval(x, 1, rw, rh);
                    g.fillRect(x + (bw / 2), y - 6, bw, th);
                    g.fillOval(x, y - 3, rw, rh);
                    g.fillOval(x, y + 5, rw, rh);
                    g.fillOval(x + 16, 1, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y - 6, bw, th);
                    g.fillOval(x + 16, y - 3, rw, rh);
                    g.fillOval(x + 16, y + 5, rw, rh);
                    
                    g.setColor(Color.RED);
                    g.fillOval(x - 16, 20, rw, rh);
                    g.fillRect((x - 16) + (bw / 2), y + 14, bw, th);
                    g.fillOval(x - 16, y + 18, rw, rh);
                    g.fillOval(x - 16, y + 24, rw, rh);
                    g.fillOval(x, 20, rw, rh);
                    g.fillRect(x + (bw / 2), y + 14, bw, th);
                    g.fillOval(x, y + 18, rw, rh);
                    g.fillOval(x, y + 24, rw, rh);
                    g.fillOval(x + 16, 20, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 14, bw, th);
                    g.fillOval(x + 16, y + 18, rw, rh);
                    g.fillOval(x + 16, y + 24, rw, rh);
                    
                    g.setColor(Color.BLUE);
                    g.fillOval(x - 16, 39, rw, rh);
                    g.fillRect((x -16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x - 16, y + 36, rw, rh);
                    g.fillOval(x - 16, y + 45, rw, rh);
                    g.fillOval(x, 39, rw, rh);
                    g.fillRect(x + (bw / 2), y + 33, bw, th);
                    g.fillOval(x, y + 36, rw, rh);
                    g.fillOval(x, y + 45, rw, rh);                    
                    g.fillOval(x + 16, 39, rw, rh);
                    g.fillRect((x + 16) + (bw / 2), y + 33, bw, th);
                    g.fillOval(x + 16, y + 36, rw, rh);
                    g.fillOval(x + 16, y + 45, rw, rh);  
                    
                    g.setColor(Color.WHITE);
                    g.drawLine(31, 3, 31, 27);
                    g.drawLine(47, 3, 47, 27);
                    g.drawLine(63, 3, 63, 27);
                    g.drawLine(31, 16, 31, 36);
                    g.drawLine(47, 16, 47, 36);
                    g.drawLine(63, 16, 63, 36);
                    g.drawLine(31, 35, 31, 57);
                    g.drawLine(47, 35, 47, 57);
                    g.drawLine(63, 35, 63, 57);
                    
                   
                    
                    break;
            } 
               
        }
    }
    
   /* public static void main(String[] args)
	{
            JFrame frame = new JFrame();

            frame.setLayout(new FlowLayout());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setTitle("Bamboo Tiles");

            frame.add(new BambooTile(2));
            frame.add(new BambooTile(3));
            frame.add(new BambooTile(4));
            frame.add(new BambooTile(5));
            frame.add(new BambooTile(6));
            frame.add(new BambooTile(7));
            frame.add(new BambooTile(8));
            frame.add(new BambooTile(9));

            frame.pack();
            frame.setVisible(true);
	}*/
}
