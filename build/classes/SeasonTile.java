import java.awt.*;
import javax.swing.*;

public class SeasonTile extends PictureTile
{
    private ImageIcon icon;
    private String name;
    private Image image;
    private Image newimg;
    
    public SeasonTile(String name)
    {
        super(name);
        this.name = name;         
    }
    
    @Override
    public void paintComponent(Graphics g)
    {
        super.paintComponent(g);
        
        switch(name)
        {
            case "Spring":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Spring.png"));
                break;
                
            case "Summer":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Summer.png"));
                break;
                
            case "Winter":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Winter.png"));
                break;
                
            case "Fall":
                icon = new ImageIcon(getClass().getClassLoader().getResource("resources/images/Fall.png"));
                break;
        }
        
        image = icon.getImage(); 
        newimg = image.getScaledInstance(50, -1,  java.awt.Image.SCALE_SMOOTH); 
        icon = new ImageIcon(newimg);  
        icon.paintIcon(this, g, 22, 6);
    }
    
    /*public static void main(String[] args)
    {
        JFrame	frame = new JFrame();

        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Season Tiles");

        frame.add(new SeasonTile("Spring"));
        frame.add(new SeasonTile("Summer"));
        frame.add(new SeasonTile("Fall"));
        frame.add(new SeasonTile("Winter"));

        frame.pack();
        frame.setVisible(true);
    }*/
    
}
