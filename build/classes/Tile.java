import java.awt.*;
import javax.swing.*;

public class Tile extends JPanel
{
    private static final Dimension TILE_SIZE = new Dimension(76, 76);    
    private static final Polygon polygon1;
    private static final Polygon polygon2;
    private static final Polygon polygon3;
    private static final Polygon polygon4;
    private static final GradientPaint polyGrad1;
    private static final GradientPaint polyGrad2;
    private static final GradientPaint rectGrad;
    private static final Rectangle rect;
    private static final Color GRAD_COLOR;
    private Color rectColor;
    private int row;
    private int col;
    private int layer;
    public boolean isSelected = false;
    
    
    
    public Tile()
    {
        setPreferredSize(TILE_SIZE);        
        setSize(TILE_SIZE);
        setOpaque(false);       
    }
    
    public int getRow()
    {
        return row;
    }
    
    public void setRow(int r)
    {
        row = r;
    }
    
    public int getCol()
    {
        return col;
    }
    
    public void setCol(int c)
    {
        col = c;
    }
    
    public int getLayer()
    {
        return layer;
    }
    
    public void setLayer(int l)
    {
        layer = l;
    }   
    
    public void setSelectedColor()
    {
        rectColor = Color.GRAY;
    }
    
    static
    {
        GRAD_COLOR = new Color(229, 237, 158);        
        rect = new Rectangle(16, 0, 59, 60);
        
        polygon1 = new Polygon();
        polygon1.addPoint(8, 8);
        polygon1.addPoint(16, 0);
        polygon1.addPoint(16, 60);
        polygon1.addPoint(8, 68);
        
        polygon2 = new Polygon();
        polygon2.addPoint(0, 16);
        polygon2.addPoint(8, 8);
        polygon2.addPoint(8, 68);
        polygon2.addPoint(0, 76);
        
        polygon3 = new Polygon();
        polygon3.addPoint(8, 68);
        polygon3.addPoint(16, 60);
        polygon3.addPoint(75, 60);
        polygon3.addPoint(67, 68);
        
        polygon4 = new Polygon();
        polygon4.addPoint(0, 76);
        polygon4.addPoint(8, 68);
        polygon4.addPoint(67, 68);
        polygon4.addPoint(59, 76);
        
        rectGrad = new GradientPaint(75, 4, GRAD_COLOR, 20, 50, Color.WHITE);
        polyGrad1 = new GradientPaint(6, 18, Color.GREEN, 2, 65, GRAD_COLOR);
        polyGrad2 = new GradientPaint(2, 65, GRAD_COLOR, 66, 74, Color.GREEN);        
    }
    
    @Override
    public void paintComponent(Graphics g)
    {       
        super.paintComponent(g);         
        Graphics2D g2 = (Graphics2D)g;
        
        if(isSelected == true)
        {
            g2.setPaint(Color.GRAY);
            g2.fill(rect);
        }
        else
        {
            g2.setPaint(rectGrad);
            g2.fill(rect);        
        }
            
        g2.setPaint(polyGrad1);
        g2.fill(polygon2);

        g2.setPaint(polyGrad2);
        g2.fill(polygon4);

        g.setColor(Color.WHITE);
        g.fillPolygon(polygon1);
        g.fillPolygon(polygon3);

        g.setColor(Color.BLACK);

        g2.draw(rect);        
        g.drawPolygon(polygon1);
        g.drawPolygon(polygon2);
        g.drawPolygon(polygon3);
        g.drawPolygon(polygon4); 
        
               
    }
    
    public boolean matches(Tile other)
    {
        if(this == other)
        {
            return false;
        }
        
        if(this.getClass() == other.getClass())
        {
            return true;
        }
        
        return false;
    }
    
    protected void grayOutSelected(Graphics g)
    {
        Graphics2D g2 = (Graphics2D)g;
        
        g.setColor(Color.GRAY);
        g2.fill(rect);
        g2.fill(polygon1);
        g2.fill(polygon2);
        g2.fill(polygon3);
        g2.fill(polygon4);
    }
    
    /*public static void main(String[] args)
    {
        JFrame frame = new JFrame();

        frame.setLayout(new FlowLayout());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setTitle("Tile");
        frame.setBackground(Color.GREEN);

        frame.add(new Tile());        
        
        frame.pack();
        frame.setVisible(true);
    }*/
}
